public class node{//This is the class that is used in the binary tree that builds and defines the nodes

boolean answer;

String string;

node left;

node right;

        public node(boolean x, String y){ //This is the constructor for the node that will take in the arguments and assign them

        answer = x;//This argument in the node is what determines if it is a answer or question (true = answer, false = question)

        string = y;//This is the string that goes in the node there should only be the answer or questions here

        left=right=null;//sets the child nodes to null
	
	}

	public node(){//This a constructor for when the class gets called with no arguments this part of code is only used temporarily since the answer and the string values will get overwritten

	answer = false;//This defaults the boolean to false so it can treat it as a question since a answer would mess up the code since it dosent want any child nodes

	string = "";//empty string

	left=right=null;

	}
	
}

