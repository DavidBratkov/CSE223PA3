/*
By: David Bratkov

Class: CSE223

Date: 5/19/2020

Description: This program will first run and build a binary tree using a file depending if you give it an argument or not it will either default to "20Q.txt" or any file name you specify while executing it. then when the desision tree is built it would start the question game where it asks the user to think of a object and tries to guess it, if it gets it right then it closes but if it gets it wrong it allows the user to modify the database and add the object's name with a yes or no question that relates to it. What the user types in can look confusing if they didnt follow the direction and ran the program with the updated database

*/

import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

public class PA3{

public static void main(String[] args){

Scanner scan = null;//decalarations here so they can work outside the try catch block

node root = new node();

try{

File filenew;

		if(args.length == 1){//This will check to see if there is any arguments passed when running the executable and try to input it as the file name

		filenew = new File(args[0]);
	
		//exist = true; // Idk why my compiler compiled and didnt tell me this was here so i didnt realise it existed *pun*

		}

		else{//This will default to look for the file "20Q.txt" when there are no aruments passed
		
		filenew = new File("20Q.txt");

		}

	scan = new Scanner(filenew);

	root = ingest(scan,root);

//print(root);

System.out.println();//print block of text
System.out.println();
System.out.println("Hello and welcome to the 20 questions game!");
System.out.println();
System.out.println();
System.out.println("Think of a object and type 'yes' or 'no' to each question");
System.out.println("type anything and press enter when ready!");

Scanner input = new Scanner(System.in);//new scanner for the user input

input.nextLine();//this waits untill the user has inputed something

String record;

input.reset();//This is needed to clear the scanner so I can grab input again

boolean game = true;

	while(game){//The while loop that runs until the game is considered finished

	String object;

	String question;

	System.out.println(root.string + "?");//First question asked

	record = input.nextLine();//requesting the user input and waiting
	
	input.reset();

		if(record.equals("no") && root.right != null){//when the user answer no the tree moves to the right
	
		root = root.right;
		
		}

		if(record.equals("yes") && root.left != null){//When the user answers yes the tree moves left

		root = root.left;

		}

		if((root.left == null && root.right == null) || (root.left == null && root.right != null)){//This checks to see if they are at a answer that has no more paths so it will "try" to guess what the user is thinking

		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("OH I think I might know!");
		System.out.println("Is it " + root.string + "?");
	
		record = input.nextLine();
	
		input.reset();

			if(record.equals("yes")){//Guessed it correctly and thanks them and closes the game
				
			System.out.println("Nice I have succesfully guessed your object!");
			System.out.println("Thank you for playing!");
	
			game = false;

			}

			if(record.equals("no")){//Where the hard part comes in

			System.out.println("What object where you thinking about?");

			object = input.nextLine();//stores the object they had in mind

			input.reset();

			System.out.println("Please type a yes or no question that will best describe your object");

			question = input.nextLine();//stores the question they typed up for their specific object

			input.reset();
		
			Scanner scan2 = null;
	
			String[] lines = new String[39783];//Made it this big so it can store all the lines in big file

			int count = 0;

			int error = 0;

			boolean check = true;
		
			scan2 = new Scanner(filenew);				
		
				while(scan2.hasNextLine()){//Here it scans the entire file and stores it all into the lines string array				
				
				lines[count] = scan2.nextLine();

					if(lines[count] == root.string) error = count;

				count = (count + 1);

				}

			filenew.delete();//Here we delete the file and make a new one so that it can be modified

			filenew.createNewFile();

			PrintWriter writer = new PrintWriter(filenew);

				for(int i = 0;i < count;i++){//This for loop will put everything back but check where the error was and input whatever the user typed in as the answer
//I did this because I couldnt figure out how to write to a specific line with PrintWriter so I remade the file with everything in it and added in the part that the user wants to add where it should go
				writer.println(lines[i]);

				//System.out.println("DEBUG i=" + i + "root=" + root.string +"array size=" + count + "lines + 2=" + lines[i+2]);
			
					if(check){//This is needed so it wont throw a error for checking a string that dosent exist

						if(lines[i+2].equals(root.string)){//here it checks 2 lines ahead of what the user said was the wrong answer and will input a question and a answer in the text file

						writer.println("Q:");		

						writer.println(question);

						writer.println("A:");
		
						writer.println(object);	
	
						check = false;
						
						}
				
					}
	
				}

			game = false;	

			writer.flush();

			writer.close();//This is needed to have a functioning file
	
			}
		
		}


	}

}

catch(FileNotFoundException error){//This would only be activated if it dosent find the file
	
	System.out.println("Error! File given =" + args[0]);

}

catch(IOException error){

	System.out.println("It errored out with the PrintWriter!");

}

}//end of main

public static node ingest(Scanner s,node p){//recursive function that will read the input of the file and build the binary desision tree

node temp;

if(s.hasNext()){

		if(s.nextLine().equals("Q:")){//Here it will see if there is a question and then build the node and then call this function again to the left and right of the node just made

		temp = new node(false,s.nextLine());

		p = temp;

		p.left = ingest(s,p.left);

		p.right = ingest(s,p.right);

		return(p);

		}

	temp = new node(true,s.nextLine());//This is when it dosent find a question and that means it would be an answer so it makes a node with both null left and right and then returns it	
	
	p = temp;
	
	return(p);	
	
}

return(p);

}

public static void print(node yuh){//Just a debug function dont worry about this

	System.out.println(yuh.string);

	if(yuh.left != null) print(yuh.left);

	if(yuh.right != null) print(yuh.right);

}

}//end of class
